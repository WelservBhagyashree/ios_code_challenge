//
//  RealmManager.swift
//  iOS_Code_Challenge
//
//  Created by WSLT82 on 24/10/20.
//  Copyright © 2020 khotbhagya13. All rights reserved.
//
/*
 
 Important : This class is used to manage all realm database related queries like save, delete, update and fetch, The conditions are added to avoid crashing issues
 
 */


import UIKit
import Realm
import RealmSwift

class RealmManager: NSObject {
    
    var realm : Realm! // Invoke migration block if needed
    
    var databaseDeleted = false

    override init() {
        let config = Realm.Configuration(deleteRealmIfMigrationNeeded: true)
        Realm.Configuration.defaultConfiguration = config
        realm = try! Realm()
    }
    
    /**
     Delete local database
     */
    func deleteDatabase() {
        try! realm.write({
            realm.deleteAll()
        })
    }
    
    /**
     Save array of objects to database
     */
    func saveObjects(objs: [Object]) {
//        try! realm.write({
            // If update = true, objects that are already in the Realm will be
            // updated instead of added a new.
        if self.databaseDeleted == false  {

            objs.forEach({ (obj) in
                if obj.isInvalidated == false {
//                    realm.add(obj, update: true)
                    realm.add(obj, update: (Realm.UpdatePolicy.init(rawValue: 1) ?? Realm.UpdatePolicy(rawValue: 1))!)
                }
            })
        }
//        })
    }

    func saveSingleObject(obj: Object) {
//        try! realm.write({
            // If update = true, objects that are already in the Realm will be
            // updated instead of added a new.
        if self.databaseDeleted == false && !obj.isInvalidated {

//            realm.add(obj, update: true)
            realm.add(obj, update: (Realm.UpdatePolicy.init(rawValue: 1) ?? Realm.UpdatePolicy(rawValue: 1))!)
        }
//        })
    }

    
    /**
     Returs an array as Results<object>?
     */
    func getObjects(type: Object.Type) -> Results<Object>? {
        return realm.objects(type)
    }
    
    
    func deleteObject(obj: Object)  {
        try! realm.write {
            realm.delete(obj)
        }
    }
    
    func deleteAllObjects(type: Object.Type) {
        try! realm.write {
            realm.delete(realm.objects(type))
        }
        
    }
}
