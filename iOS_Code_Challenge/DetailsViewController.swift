//
//  DetailsViewController.swift
//  iOS_Code_Challenge
//
//  Created by WSLT82 on 24/10/20.
//  Copyright © 2020 khotbhagya13. All rights reserved.
//

import UIKit
import SDWebImage

class DetailsViewController: UIViewController {

    var selectedData = DataDetails()
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //        if selectedData.type == "text"{
        let type = UILabel(frame: CGRect(x: 20, y: 100, width: (ScreenSize.SCREEN_WIDTH - 50), height: 25))
        type.textAlignment = .left
        type.text = "Type : " + selectedData.type
        type.font = UIFont.boldSystemFont(ofSize:25)
        type.tintColor = .blue
        
        let labelDate = UILabel(frame: CGRect(x: 20, y: 130, width: (ScreenSize.SCREEN_WIDTH - 50), height: 25))
        labelDate.textAlignment = .left
        labelDate.text = "Date : " + selectedData.date
        labelDate.font = UIFont.boldSystemFont(ofSize: 17)
        labelDate.tintColor = .blue
        
        var textHeight = selectedData.data.heightWithConstrainedWidth((ScreenSize.SCREEN_WIDTH - 50), font: UIFont.boldSystemFont(ofSize: 17))
        let labelData = UILabel(frame: CGRect(x: 20, y: 160 , width: (ScreenSize.SCREEN_WIDTH - 50), height: textHeight))
        labelData.textAlignment = .left
        labelData.text = "Data : " + selectedData.data
        labelData.font = UIFont.boldSystemFont(ofSize: 17)
        labelData.tintColor = .black
        labelData.numberOfLines = 0
        labelData.lineBreakMode = .byWordWrapping
        labelData.textColor = .red
        
        let urlImage = UIImageView(frame: CGRect(x: 0 , y: 150, width: 100, height: 100))//(frame: labelData.frame.width  , y: 150, width: 100, height: 100))
        urlImage.contentMode = .scaleToFill // without this your image will shrink and looks ugly
        urlImage.translatesAutoresizingMaskIntoConstraints = false
        urlImage.layer.cornerRadius = 5
        urlImage.clipsToBounds = false
        if let url = URL(string: selectedData.data) {
            urlImage.isHidden = false
            urlImage.sd_setImage(with: url, placeholderImage: UIImage(named: "image-placeholder"), options: SDWebImageOptions.refreshCached, completed: nil)
        }else {
            urlImage.isHidden = true
            urlImage.image = nil
        }
        // add UI elements to view
        self.view.addSubview(urlImage)
        self.view.addSubview(type)
        self.view.addSubview(labelDate)
        self.view.addSubview(labelData)
        
        // add constraint to imageview
        NSLayoutConstraint.activate([
            urlImage.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            urlImage.widthAnchor.constraint(equalToConstant: 300),
            urlImage.heightAnchor.constraint(equalToConstant: 300),
            urlImage.topAnchor.constraint(equalTo: labelData.bottomAnchor, constant: 20),
            //constraint(equalToConstant: 125)
        ])
    }
}
