//
//  RealmObject.swift
//  iOS_Code_Challenge
//
//  Created by WSLT82 on 24/10/20.
//  Copyright © 2020 khotbhagya13. All rights reserved.
//

import UIKit
import Realm
import RealmSwift
import SwiftyJSON

// Global instance of realm manager to use it whereever necessary
let realm = RealmManager()

// This realm object class used to store user details received from server after login/signup/profile update


class DataDetails: Object {
    
    @objc dynamic var offlineId : String = ""
    @objc dynamic var id : String = ""
    @objc dynamic var type : String = ""
    @objc dynamic var date : String = ""
    @objc dynamic var data : String = ""
    @objc dynamic var isSynced = false
    
    class func getDataList(_ array: [JSON], isSynced : Bool) -> [DataDetails] {
        
        var events = [DataDetails]()
        array.forEach { (data) in
            
            var d = DataDetails()
            
            if let id = data[APIKey.id].string {
                d.id = id
            }
            if let type = data[APIKey.type].string {
                d.type = type
            }
            if let date = data[APIKey.date].string {
                d.date = date
            }
            if let data = data[APIKey.data].string {
                d.data = data
            }
            
            d.isSynced = isSynced
            
            events.append(d)
        }
         return events
    }
}
