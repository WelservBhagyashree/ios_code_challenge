//
//  Structs.swift
//  iOS_Code_Challenge
//
//  Created by WSLT82 on 24/10/20.
//  Copyright © 2020 khotbhagya13. All rights reserved.
//

import Foundation
import UIKit

// MARK:- API REQUEST & RESPONSE KEY
struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    
    static func topEdge()->CGFloat
    {
        
        if #available(iOS 11.0, *) {
            let insets = UIApplication.shared.delegate?.window??.safeAreaInsets
            return (insets?.top)! + 20
        }
        return 20
        
        
    }
}
struct APIKey {
    
    static let id = "id"
    static let type = "type"
    static let date = "date"
    static let data = "data"
    static let offlineId = "offlineId"
}


